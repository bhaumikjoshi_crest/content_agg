from flask import Flask, url_for, render_template, request, session
import json
import requests
from bs4 import BeautifulSoup
import feedparser
import praw, pprint
import tweepy
import re
from datetime import date
from dateutil.relativedelta import relativedelta

try:
    from googlesearch import search
except ImportError:
    print("No module named 'google' found")


app = Flask(__name__)
sites = []
app.secret_key = "Flask"
cache = {}


class SoArticle:
    def __init__(self, question, answer, link, header):
        self.question = question
        self.answer = answer
        self.link = link
        self.header = header
        self.type = "so"


class MediumArticle:
    def __init__(self, title, summary, link):
        self.title = title
        self.link = link
        self.summary = summary
        self.type = "md"


# class RedditArticle:
#     def __init__(self, title, summary, link):
#         self.title = title
#         self.link = link
#         self.summary = summary
#         self.type = "md"


class GFGArticle:
    def __init__(self, title, summary, link):
        self.title = title
        self.link = link
        self.summary = summary
        self.type = "gfg"


class Tweet:
    def __init__(self, id, text, user, tags):
        self.id = id
        self.text = text
        self.user = user
        self.tags = tags


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        query = request.form["query"]
        if query == "":
            return "Invalid query"
        session["query"] = query
        global sites
        if sites == []:
            with open("static/json/sites.json", "r") as f:
                sites = json.load(f)
        return render_template("index.html", sites=sites, query=query)

    return render_template("index.html")


def get_question(content):
    soup = BeautifulSoup(content, "html.parser")
    head = soup.find(id="question-header").find("h1").text
    que = soup.select(".question")
    lst = []
    for q in que:
        lst.append(q)
    el = lst[0]
    some = el.findChildren()
    for a in some:
        try:
            if "js-post-body" in a["class"]:
                return a.text, head
        except KeyError:
            pass


def get_answer(content):
    soup = BeautifulSoup(content, "html.parser")
    ans = soup.select(".answer")
    lst = []
    for a in ans:
        lst.append(a)
    el = lst[0]
    some = el.findChildren()
    for a in some:
        try:
            if "js-post-body" in a["class"]:
                return a.text
        except KeyError:
            pass


def create_article(content, link, code):
    if code == "so":
        q, h = get_question(content)
        an = get_answer(content)
        return SoArticle(q, an, link, h)
    if code == "md":
        ans = []
        soup = BeautifulSoup(content, "lxml")
        xx = soup.find_all("p")
        for i in range(5):
            ans.append((xx[i].text))
        ans = "".join(ans)
        return MediumArticle(soup.title.text, ans, link)
    # if code == "re":
    #     ans = []
    #     soup = BeautifulSoup(content, "lxml")
    #     xx = soup.find_all("p")
    #     for i in range(4):
    #         ans.append((xx[i].text))
    #     ans = "".join(ans)
    #     return RedditArticle()
    if code == "gfg":
        ans = []
        soup = BeautifulSoup(content, "lxml")
        summary = soup.find_all("p")
        title = soup.find("title")
        print(title)
        for i in range(10):
            try:
                ans.append((summary[i].text))
            except IndexError:
                break
        ans = "".join(ans)
        return GFGArticle(title.text, ans, link)


def scrap_tweets(words):
    consumer_key = "2p8GRJGlAt5VQU4GjjbEOJK14"
    consumer_secret = "uEpuiIKKAJN28ajRy6IPEJSUzU6WKaTzXw2PionNAS2iegfocV"
    access_key = "1346423498890100736-xDxnazxSnJzgWeBwpPN9jIgVPswuQ3"
    access_secret = "hab0kkgthZ3Ebf5urELEeY7dwvnabNOzuaHFmrWO9DdfF"
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_key, access_secret)
    api = tweepy.API(auth)
    date_since = date.today() - relativedelta(months=6)  # Last 6 months
    numtweet = 9

    tweets = tweepy.Cursor(
        api.search, q=words, lang="en", since=date_since, tweet_mode="extended"
    ).items(numtweet)

    list_tweets = [tweet for tweet in tweets]
    lst = []
    for tweet in list_tweets:
        id = tweet.id
        username = tweet.user.screen_name
        hashtags = tweet.entities["hashtags"]

        # Retweets can be distinguished by a retweeted_status attribute,
        # in case it is an invalid reference, except block will be executed
        try:
            text = tweet.retweeted_status.full_text
        except AttributeError:
            text = tweet.full_text
        hashtext = list()
        for j in range(0, len(hashtags)):
            hashtext.append("#" + hashtags[j]["text"])
        hashtext = " ".join(hashtext)
        # print(text)
        lst.append(Tweet(id, text, username, hashtext))
    return lst


@app.route("/prev")
def get_prev():
    i = session["i"]
    lst = session["lst"]
    i -= 1
    session["i"] = i
    at = lst[i]
    print(i)
    return render_template("search.html", article=at, index=i, max_ind=len(lst))


@app.route("/next/<code>")
def get_next(code):
    i = session["i"]
    i += 1
    session["i"] = i
    cache_i = session["fetched"]
    lst = session["lst"]
    if i <= cache_i:
        at = lst[i]

    else:
        content = requests.get(lst[i]).text
        at = create_article(content, lst[i], code)
        session["fetched"] = i
        lst[i] = at.__dict__
        session["lst"] = lst
        cache_i += 1
        session["fetched"] = cache_i
    print(i)
    print(cache_i)
    return render_template("search.html", article=at, index=i, max_ind=len(lst))


@app.route("/search/<site_code>")
def search_site(site_code):

    if site_code == "md":
        return content_medium()
    if site_code == "yo":
        return content_youtube()
    if site_code == "tw":
        return content_twitter()
    site_url = sites[site_code]

    query = f"site:{site_url} {session['query']}"
    # print(query)
    res = []
    for j in search(query, tld="co.in", num=5, stop=5, pause=1):
        if site_code == "so" and "tagged" in j:
            pass
        else:
            res.append(j)

    print(res)
    q = session["query"]
    content = requests.get(res[0]).text
    at = create_article(content, res[0], site_code)
    # ans.findChildren
    # print(a)
    session["i"] = 0
    session["fetched"] = 0
    session["code"] = site_code
    session["lst"] = [at.__dict__] + res[1:]
    return render_template("search.html", article=at, index=0, max_ind=len(res))


def content_twitter():
    q = session["query"]
    tweets = scrap_tweets(q)
    return render_template("tweets.html", tweets=tweets)


def content_medium():
    demo1 = []
    q = session["query"]
    q = q.replace(" ", "")
    feed_python = feedparser.parse(f"https://medium.com/feed/tag/{q}")
    # print("Python Today: ")
    for i in range(5):
        try:
            entry = feed_python.entries[i]
            demo1.append(entry.link)
        except IndexError:
            if i == 0:
                return "No content"
        # demo.append(entry.title)
        # demo2.append(entry.summary)
    # mylist = zip(demo, demo1, demo2)
    content = requests.get(demo1[0]).text
    at = create_article(content, demo1[0], "md")
    # ans.findChildren
    # print(a)
    session["i"] = 0
    session["fetched"] = 0
    session["lst"] = [at.__dict__] + demo1[1:]
    return render_template("search.html", article=at, index=0, max_ind=len(demo1))


def content_reddit():
    reddit = praw.Reddit(
        client_id="qgAlkqcVulHzQw",
        client_secret="XOUGEq7oe-a7WVoBSSwnrZx6bdaFcA",
        grant_type_access="client_credentials",
        user_agent="ronak",
    )

    posts_url = []
    q = session["query"]
    ml_subreddit = reddit.subreddit(q)
    for post in ml_subreddit.hot(limit=4):
        posts_url.append(post.url)
    content = requests.get(posts_url[0]).text
    at = create_article(content, posts_url[0], "md")
    # print(posts_url)
    # soup = BeautifulSoup(content, "lxml")
    # xx = soup.find_all("p")
    # for i in range(4):
    #    ans = ans + (xx[i].text)
    session["i"] = 0
    session["fetched"] = 0
    session["lst"] = [at.__dict__] + posts_url[1:]
    return render_template("search.html", article=at, index=0, max_ind=len(posts_url))


def content_youtube():

    ans = []
    check = "https://www.youtube.com/"
    s = ""
    q = session["query"]
    query = f"https://www.youtube.com/{q}"
    regex = re.compile("[@!#$%^&*()<>?\|}{~]")
    for j in search(query, tld="co.in", num=10, stop=10, pause=2):
        print(j)
        ans1 = j[0:24]
        print(ans1)
        if regex.search(j) == None and ans1 == check:
            ss = j.split("/")
            temp = len(ss)
            s = ss[temp - 1]
            print(s + "\n")
            ans.append(s)
        elif ans1 == check:
            s = j[32:]
            print(s + "in" + "\n")
            ans.append(s)
    return render_template("youtube.html", context=ans)


if __name__ == "__main__":
    app.run(debug=True)